package ru.acmg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReminderBotApp {
    public static void main(String[] args) {
        SpringApplication.run(ReminderBotApp.class, args);
    }
}
